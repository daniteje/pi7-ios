//
//  Generic-Service.swift
//  GoodPractices
//
//  Created by Dani Teje on 26/03/2021.
//  Copyright © 2021 Dani Teje. All rights reserved.
//

import Foundation

class GenericService {
  func update_required(version: String, completionHandler: @escaping ((Result<UpdateRequired, API.NetworkError>) -> Void)) {
    Cache.logOut()
    
    let parameters: [String: Any] = ["platform" : "ios", "version" : version]

    API.shared.callDecoding(GenericAPI.update_required(parameters), of: UpdateRequired.self, completion: completionHandler)
  }
}
