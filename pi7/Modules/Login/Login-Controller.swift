//
//  Login-Controller.swift
//  pi7
//
//  Created by Dani Tejedor on 25/03/2021.
//  Copyright © 2021 Dani Teje. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
  // MARK: - IBOutlets
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var emailTextField: CustomTextField!
  @IBOutlet weak var passwordTextField: CustomTextField!
  @IBOutlet weak var forgotPasswordButton: UIButton!
  @IBOutlet weak var loginButton: CustomButton!
  @IBOutlet weak var notSignedUpLabel: UILabel!
  @IBOutlet weak var signUpButton: UIButton!

  // MARK: - Life cycle
  override func viewDidLoad() {
    super.viewDidLoad()

    configureTextFields()
    setupUI()
    fillUI()
  }

  // MARK: - Functions
  func setupUI() {
    titleLabel.text = String.Login.Welcome.localized
    emailTextField.placeholder = String.Common.Mail.localized
    passwordTextField.placeholder = String.Common.Password.localized
    forgotPasswordButton.setTitle(String.Login.ForgotPassword.localized, for: .normal)
    loginButton.setTitle(String.Login.Button.localized, for: .normal)
    notSignedUpLabel.text = String.Login.RegisterText.localized
    signUpButton.setTitle(String.Login.Register.localized.lowercased(), for: .normal)
  }

  func fillUI() {
    if !isViewLoaded { return }

    emailTextField.textField.text = "dani@pi7.app"
    passwordTextField.textField.text = "12345678Aa@"
  }

  func configureTextFields() {
    emailTextField.addErrorsToCheck([TextFieldErrorEmptyValue(),
                                     TextFieldErrorEmailFormat()])
    emailTextField.textField.textContentType = .emailAddress
    emailTextField.textField.keyboardType    = .emailAddress

    passwordTextField.addErrorsToCheck([TextFieldErrorEmptyValue(),
                                        TextFieldErrorMinimumCharacters(8)])
    passwordTextField.textField.textContentType = .password

  }

  func textFieldsHaveErrors() -> Bool {
    var textFieldsHaveErrors = false

    if emailTextField.hasError    { textFieldsHaveErrors = true }
    if passwordTextField.hasError { textFieldsHaveErrors = true }

    return textFieldsHaveErrors
  }


  // MARK: - Observers
  @IBAction func loginButtonPressed(_ sender: UIButton) {
    if textFieldsHaveErrors() { return }

    let tabBarVC = UIViewController.instantiate(viewController: TabBarViewController.self)
    changeRoot(to: tabBarVC)
  }

  @IBAction func recoverPasswordButtonPressed(_ sender: UIButton) {
    let recoverPasswordVC = UIViewController.instantiate(viewController: RecoverPasswordViewController.self)
    present(viewController: recoverPasswordVC)
  }

  @IBAction func signUpButtonPressed(_ sender: UIButton) {
    let signUpVC = UIViewController.instantiate(viewController: RegisterViewController.self)
    present(viewController: signUpVC)
  }
}
