//
//  Status-Controller.swift
//  pi7
//
//  Created by Dani Tejedor on 27/3/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import Foundation

class StatusViewController: ViewController, ViewModelController {
  typealias T = StatusViewModel

  // MARK: - IBOutlets

  // MARK: - Properties
  var viewModel: StatusViewModel!

  // MARK: - Life cycle
  override func viewDidLoad() {
    super.viewDidLoad()

    fillUI()
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    navigationItem.largeTitleDisplayMode = .always
  }

  // MARK: - Functions
  func fillUI() {
    if !isViewLoaded { return }

    navigationItem.title = "Status"
    navigationController?.navigationBar.prefersLargeTitles = true

  }

  // MARK: - Observers
}
