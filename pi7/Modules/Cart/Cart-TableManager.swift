//
//  Cart-TableManager.swift
//  pi7
//
//  Created by Dani Tejedor on 27/3/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import UIKit

extension CartViewController: UITableViewDelegate, UITableViewDataSource {
  func configure(_ tableView: UITableView) {
    tableView.delegate    = self
    tableView.dataSource  = self
    
    tableView.register(CartCell.self)
    tableView.register(header: CartCategoryTableViewHeader.self)
  }

  func numberOfSections(in tableView: UITableView) -> Int {
    viewModel.showingCart.count + 1
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let categoryName = section == viewModel.showingCart.count ? "Ya comprado" :  viewModel.showingCart[section].sectionName
    let categoryVM = CartCategoryTableViewHeaderViewModel(title: categoryName)
    return tableView.dequeue(header: CartCategoryTableViewHeader.self, viewModel: categoryVM)
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return section == viewModel.showingCart.count ?
      viewModel.checkedCart.products.count : viewModel.showingCart[section].products.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let product = indexPath.section == viewModel.showingCart.count ? viewModel.checkedCart.products[indexPath.row] : viewModel.showingCart[indexPath.section].products[indexPath.row]
    let cellVM = CartCellViewModel(product: product)
    return tableView.dequeue(CartCell.self, viewModel: cellVM)
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if(indexPath.section == viewModel.showingCart.count){
      let product = viewModel.checkedCart.products[indexPath.row]
      product.isChecked = false

      viewModel.checkedCart.products.remove(at: indexPath.row)

      if let sectionIndex = viewModel.showingCart.firstIndex(where: { $0.sectionID == product.sectionID }) {
        viewModel.showingCart[sectionIndex].products.append(product)
      } else {
        if let section = viewModel.cart.first(where: {$0.sectionID == product.sectionID}) {
          section.products = [product]
          viewModel.showingCart.append(section)
        }
      }
    } else {
      let section = viewModel.showingCart[indexPath.section]
      let product = section.products[indexPath.row]
      product.isChecked = true

      section.products.remove(at: indexPath.row)
      
      viewModel.checkedCart.products.append(product)
      if (section.products.contains(where: {!$0.isChecked} )) {
        viewModel.showingCart[indexPath.section] = section
      } else {
        viewModel.showingCart.remove(at: indexPath.section)
      }
    }
    tableView.reloadData()
  }
}
