//
//  Cart-ViewController.swift
//  pi7
//
//  Created by Dani Tejedor on 26/3/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import UIKit

class CartViewController: ViewController, ViewModelController, AddProductDelegate {
  typealias T = CartViewModel

  // MARK: - IBOutlets
  @IBOutlet weak var tableView: UITableView!

  // MARK: - Properties
  var viewModel: CartViewModel!

  override var useLargeTitle: Bool {
    return true
  }

  // MARK: - Life cycle
  override func viewDidLoad() {
    super.viewDidLoad()

    setupUI()
    configure(tableView)
  }

  // MARK: - Functions
  func setupUI() {
    let rightButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
    rightButton.tintColor = UIColor(named: "pink")
    navigationItem.rightBarButtonItem = rightButton
    navigationItem.title = String.Cart.Title.localized
  }

  func addProduct(product: Product) {
    if let sectionIndex = viewModel.cart.firstIndex(where: { $0.sectionID == product.sectionID} ) {
      viewModel.cart[sectionIndex].products.append(product)
    } else {
      let generalSection = CartSection(sectionID: 0, sectionName: String.Common.General.localized, products: [product])
      viewModel.cart.insert(generalSection, at: 0)
      viewModel.showingCart.insert(generalSection, at: 0)
    }
    tableView.reloadData()
  }

  // MARK: - Observers
  @objc func addTapped(){
    let addProductVM = AddProductViewModel()
    let addProductViewController = ViewController.instantiate(viewController: AddProductViewController.self,
                                                              withViewModel: addProductVM)
    addProductViewController.delegate = self
    present(addProductViewController, animated: true)
  }
}
