//
//  Cart-ViewModel.swift
//  pi7
//
//  Created by Dani Tejedor on 26/3/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import Foundation

class CartViewModel: ViewModel {
  //MARK: - Properties
  var checkedCart: CartSection = CartSection(sectionID: 0, sectionName: "Checked", products: [])
  var showingCart: [CartSection] = []

  var _cart: Dynamic<[CartSection]> = Dynamic([])
  var cart: [CartSection] {
    get { _cart.value }
    set { _cart.value = newValue }
  }

////  var cart: [CartSection] {
////    didSet {
////      cart.forEach { section in
////        let groups = section.products.separate(predicate: { $0.isChecked } )
////        checkedCart.products.append(contentsOf: groups.matching)
////        let newSection = section
////        newSection.products = groups.notMatching
////        showingCart.append(section)
////
////
////      }
////    }
//  }



  // {
//    didSet {
//      cart.forEach { section in
//        let groups = section.products.separate(predicate: { $0.isChecked } )
//        checkedCart.products.append(contentsOf: groups.matching)
//        let newSection = section
//        newSection.products = groups.notMatching
//        showingCart.append(section)

  //      showingCart.append(contentsOf: newSection.products)
  //      showingCart.append(CartSection(newSection))
  //      newSection.products = groups.notMatching
  //      showingCart[section.sectionID-1].products.append(contentsOf: groups.matching) //= groups.notMatching
//      }
//    }

//  var cart: [CartSection] {
//    didSet {
//
//    }
//  }
//  var checkedSections: [CartSection] { }

  //MARK: - Init
  init(){
    self.cart = [section1, section2, section3, section4]
    cart.forEach { section in
      let groups = section.products.separate(predicate: { $0.isChecked } )
      checkedCart.products.append(contentsOf: groups.matching)
      let newSection = section
      newSection.products = groups.notMatching
      showingCart.append(section)


    }
  }
}
