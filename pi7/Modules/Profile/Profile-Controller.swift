//
//  Profile-Controller.swift
//  pi7
//
//  Created by Dani Tejedor on 27/3/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import UIKit

class ProfileViewController: ViewController, ViewModelController {
  typealias T = ProfileViewModel

  // MARK: - IBOutlets

  @IBOutlet weak var fullNameLabel: UILabel!
  @IBOutlet weak var codeLabel: UILabel!
  @IBOutlet weak var userCodeLabel: UILabel!
  @IBOutlet weak var mailLabel: UILabel!
  @IBOutlet weak var editGroupButton: CustomButton!
  @IBOutlet weak var editInfoButton: CustomButton!
  @IBOutlet weak var editPasswordButton: CustomButton!

  // MARK: - Properties
  var viewModel: ProfileViewModel!

  override var useLargeTitle: Bool {
    return true
  }
  // MARK: - Life cycle
  override func viewDidLoad() {
    super.viewDidLoad()

    setupUI()
  }

  // MARK: - Functions
  func setupUI() {
    navigationItem.title = String.TabBar.Profile.localized
    
    codeLabel.text = String.Profile.Code.localized
    editGroupButton.setTitle(String.Profile.EditGroup.localized, for: .normal)
    editInfoButton.setTitle(String.Profile.EditInfo.localized, for: .normal)
    editPasswordButton.setTitle(String.Profile.EditPassword.localized, for: .normal)
}

  // MARK: - Observers
}
