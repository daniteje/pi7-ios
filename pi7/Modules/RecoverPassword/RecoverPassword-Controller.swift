//
//  RecoverPassword-Controller.swift
//  pi7
//
//  Created by Dani Tejedor on 25/03/2021.
//  Copyright © 2021 Dani Teje. All rights reserved.
//

import UIKit

class RecoverPasswordViewController: UIViewController {
  // MARK: - IBOutlets
  @IBOutlet weak var infoLabel: UILabel!
  @IBOutlet weak var emailTextField: CustomTextField!
  @IBOutlet weak var recoverButton: CustomButton!

  // MARK: - Properties

  // MARK: - Life cycle
  override func viewDidLoad() {
    super.viewDidLoad()

    setupUI()
    configureTextFields()
  }

  // MARK: - Functions
  func setupUI() {
    infoLabel.text = String.RecoverPassword.Information.localized
    emailTextField.placeholder = String.Common.Mail.localized
    recoverButton.setTitle(String.RecoverPassword.Button.localized, for: .normal)
  }

  func fillUI() {
    if !isViewLoaded { return }

  }

  func configureTextFields() {
    emailTextField.addErrorsToCheck([TextFieldErrorEmptyValue(),
                                     TextFieldErrorEmailFormat()])
    emailTextField.textField.textContentType = .emailAddress
    emailTextField.textField.keyboardType    = .emailAddress
  }

  func textFieldsHaveErrors() -> Bool {
    var textFieldsHaveErrors = false

    if emailTextField.hasError    { textFieldsHaveErrors = true }

    return textFieldsHaveErrors
  }

  // MARK: - Observers
  @IBAction func recoverButtonPressed(_ sender: UIButton) {
    if textFieldsHaveErrors() { return }
  }

  @IBAction func closeButtonPressed(_ sender: UIButton) {
    dismiss(animated: true)
  }
}
