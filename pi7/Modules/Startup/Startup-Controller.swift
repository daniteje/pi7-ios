//
//  ViewController.swift
//  pi7
//
//  Created by Dani Tejedor on 25/03/2021.
//  Copyright © 2021 Dani Teje. All rights reserved.
//

import UIKit

class StartupViewController: ViewController {
  // MARK: - IBOutlets

  // MARK: - Properties

  // MARK: - Life cycle
  override func viewDidLoad() {
    super.viewDidLoad()

  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    goHome()
  }
  // MARK: - Functions
  func fillUI() {
    if !isViewLoaded { return }

  }

  func goHome() {
    let tabBarVC = UIViewController.instantiate(viewController: TabBarViewController.self)
    changeRoot(to: tabBarVC)
  }

  // MARK: - Observers
  @IBAction func registerButtonPressed(_ sender: UIButton) {
    let registerVC = UIViewController.instantiate(viewController: RegisterViewController.self)
    present(viewController: registerVC)
  }
  
  @IBAction func loginButtonPressed(_ sender: UIButton) {
    let loginVC = UIViewController.instantiate(viewController: LoginViewController.self)
    present(viewController: loginVC)
  }
}
