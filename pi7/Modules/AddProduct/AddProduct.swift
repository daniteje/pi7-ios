//
//  AddProduct.swift
//  pi7
//
//  Created by Dani Tejedor on 29/3/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import UIKit

protocol AddProductDelegate: AnyObject {
  func addProduct(product: Product)
}

class AddProductViewController: ViewController, ViewModelController, UITextFieldDelegate {
  typealias T = AddProductViewModel

  // MARK: - IBOutlets
  @IBOutlet weak var productNameLabel: UILabel!
  @IBOutlet weak var productName: UITextField!
  @IBOutlet weak var listLabel: UILabel!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var continueButton: CustomButton!

  // MARK: - Properties
  var viewModel: AddProductViewModel!
  weak var delegate: AddProductDelegate?

  // MARK: - Life cycle
  override func viewDidLoad() {
    super.viewDidLoad()

    setupUI()
    configure(tableView)
  }

  // MARK: - Functions
  func fillUI() {
    if !isViewLoaded { return }
  }

  func setupUI() {
    productNameLabel.text = String.AddProduct.NameQuestion.localized
    productName.attributedPlaceholder = NSAttributedString(string: String.AddProduct.NamePlaceHolder.localized,attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
    listLabel.text = String.AddProduct.ListQuestion.localized
    continueButton.setTitle(String.Common.Continue.localized, for: .normal)
    
    productName.delegate = self
  }

  // MARK: - Observers
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
    if !text.isEmpty{
      tableView.isUserInteractionEnabled = true
      tableView.alpha = 1.0
      listLabel.alpha = 1.0
      
    } else {
      tableView.isUserInteractionEnabled = false
      tableView.alpha = 0.5
      listLabel.alpha = 0.5
    }
    return true
  }
}

