//
//  AddProduct-ViewModel.swift
//  pi7
//
//  Created by Dani Tejedor on 29/3/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import Foundation

class AddProductViewModel: ViewModel {
  //MARK: - Properties
  let sections: [CartSection] = [generalSection, section1, section2, section3, section4]
  var selectedSectionID = 0
  //MARK: - Init
  init(){
  }
}
