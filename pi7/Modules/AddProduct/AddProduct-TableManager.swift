//
//  File.swift
//  pi7
//
//  Created by Dani Tejedor on 29/3/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import UIKit

extension AddProductViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    viewModel.sections.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cellVM = CartSectionCellViewModel(section: viewModel.sections[indexPath.row], isSelected: viewModel.selectedSectionID == indexPath.row)
    return tableView.dequeue(CartSectionCell.self, viewModel: cellVM)
  }

  func configure(_ tableView: UITableView) {
    tableView.delegate    = self
    tableView.dataSource  = self

    tableView.register(CartSectionCell.self)
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    viewModel.selectedSectionID = indexPath.row
    guard let name = productName.text else { return }
    let newProduct = Product(name: name, isChecked: false, sectionID: indexPath.row)
    delegate?.addProduct(product: newProduct)
    tableView.reloadData()

    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
      self.dismiss(animated: true)
    }
  }
}
