//
//  Register-Controller.swift
//  pi7
//
//  Created by Dani Tejedor on 25/03/2021.
//  Copyright © 2021 Dani Teje. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
  // MARK: - IBOutlets
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var nameTextField: CustomTextField!
  @IBOutlet weak var lastnameTextField: CustomTextField!
  @IBOutlet weak var emailTextField: CustomTextField!
  @IBOutlet weak var passwordTextField: CustomTextField!
  @IBOutlet weak var confirmPasswordTextField: CustomTextField!
  @IBOutlet weak var registerButton: CustomButton!

  // MARK: - Properties

  // MARK: - Life cycle
  override func viewDidLoad() {
    super.viewDidLoad()

    setupUI()
    configureTextFields()
  }

  // MARK: - Functions
  func setupUI() {
    titleLabel.text = String.Register.Title.localized
    nameTextField.placeholder = String.Common.Name.localized
    lastnameTextField.placeholder = String.Common.Lastname.localized
    emailTextField.placeholder = String.Common.Mail.localized
    passwordTextField.placeholder = String.Common.Password.localized
    confirmPasswordTextField.placeholder = String.Register.ConfirmPassword.localized
    registerButton.setTitle(String.Register.Button.localized, for: .normal)
  }

  func fillUI() {
    if !isViewLoaded { return }
  }

  func configureTextFields() {
    nameTextField.addErrorsToCheck([TextFieldErrorEmptyValue()])
    nameTextField.textField.textContentType = .name

    lastnameTextField.addErrorsToCheck([TextFieldErrorEmptyValue()])
    nameTextField.textField.textContentType = .familyName

    emailTextField.addErrorsToCheck([TextFieldErrorEmptyValue(),
                                     TextFieldErrorEmailFormat()])
    emailTextField.textField.textContentType = .emailAddress
    emailTextField.textField.keyboardType    = .emailAddress

    passwordTextField.addErrorsToCheck([TextFieldErrorEmptyValue(),
                                        TextFieldErrorMinimumCharacters(8)])
    passwordTextField.textField.textContentType = .newPassword

    confirmPasswordTextField.addErrorsToCheck([TextFieldErrorEmptyValue(),
                                        TextFieldErrorMinimumCharacters(8)])
    confirmPasswordTextField.textField.textContentType = .newPassword

  }

  func textFieldsHaveErrors() -> Bool {
    var textFieldsHaveErrors = false

    if nameTextField.hasError     { textFieldsHaveErrors = true }
    if lastnameTextField.hasError { textFieldsHaveErrors = true }
    if emailTextField.hasError    { textFieldsHaveErrors = true }
    if passwordTextField.hasError { textFieldsHaveErrors = true }

    return textFieldsHaveErrors
  }

  // MARK: - Observers
  @IBAction func registerButtonPressed(_ sender: UIButton) {
    if textFieldsHaveErrors() { return }

  }

  @IBAction func closeButtonPressed(_ sender: UIButton) {
    dismiss(animated: true)
  }
}
