//
//  Plants-ViewModel.swift
//  pi7
//
//  Created by Dani Tejedor on 27/3/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import UIKit

class PlantsViewModel: ViewModel {
  //MARK: - Properties
  var tableManager = PlantsTableManager()
  
  var weekSchedule: [Day]
  var plants: [Plant]

  var _selectedDayIndex: Dynamic<Int> = Dynamic(0)
  var selectedDayIndex: Int {
    get { _selectedDayIndex.value }
    set { _selectedDayIndex.value = newValue }
  }

  //MARK: - Init
  init(){
    self.weekSchedule = week
    self.plants = initialPlants

    self.tableManager.addTasksSection(tasks: self.weekSchedule[selectedDayIndex].tasks)
    self.tableManager.addPlantsSection(plants: self.plants)
  }
}
