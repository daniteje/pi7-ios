//
//  Plants-TableManager.swift
//  pi7
//
//  Created by Dani Tejedor on 1/4/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import UIKit

extension PlantsViewController: UITableViewDelegate, UITableViewDataSource {
  func configure(_ tableView: UITableView){
    tableView.dataSource = self
    tableView.delegate = self

    tableView.register(PlantsCalendarRow.self)
    tableView.register(PlantsTasksCell.self)
    tableView.register(PlantsTasksEmptyRow.self)
    tableView.register(PlantsListCell.self)
    tableView.register(PlantsTasksEmptyRow.self)
    tableView.register(header: CartCategoryTableViewHeader.self)
  }

  func numberOfSections(in tableView: UITableView) -> Int {
    return viewModel.tableManager.sections.count
  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    viewModel.tableManager.sections[section].sectionCells.count
  }

  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    switch viewModel.tableManager.sections[section] {
    case .plants(_):
      let name = String.Plants.MyPlants.localized
      let categoryVM = CartCategoryTableViewHeaderViewModel(title: name)
      return tableView.dequeue(header: CartCategoryTableViewHeader.self, viewModel: categoryVM)

    default:
      return nil
    }
  }

  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    let view = UIView()
    view.backgroundColor = .black
    return view
  }

  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    if case .tasks(_)  = viewModel.tableManager.sections[section] {
      return 20
    }
    return CGFloat.leastNormalMagnitude
  }

  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if case .plants(_)  = viewModel.tableManager.sections[section] {
      return 40
    }
    return CGFloat.leastNormalMagnitude
 }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    switch viewModel.tableManager.sections[indexPath.section] {
    case .calendar: break;
    case .tasks(_):
      viewModel.weekSchedule[viewModel.selectedDayIndex].tasks[indexPath.row].isDone.toggle()
    case .plants(_):
      break;
    }
    tableView.reloadData()
  }


  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    switch viewModel.tableManager.sections[indexPath.section] {
    case .calendar:
      let rowVM = PlantsCalendarRowViewModel(weekSchedule: viewModel.weekSchedule, selected: viewModel.selectedDayIndex)
      let cell = tableView.dequeue(PlantsCalendarRow.self, viewModel: rowVM)
      cell.delegate = self
      return cell

    case .tasks(let tasks):
      if tasks.isEmpty { return tableView.dequeue(PlantsTasksEmptyRow.self) }
      let rowVM = PlantsTasksCellViewModel(task: viewModel.weekSchedule[viewModel.selectedDayIndex].tasks[indexPath.row])
      let cell = tableView.dequeue(PlantsTasksCell.self, viewModel: rowVM)
      return cell

    case .plants:
      let rowVM = PlantsListCellViewModel(plant: viewModel.plants[indexPath.row])
      let cell = tableView.dequeue(PlantsListCell.self, viewModel: rowVM)
      return cell

    }
  }
}

class PlantsTableManager {
  var sections: [Section] = [.calendar, .tasks([]), .plants([])]

  enum Row {
    case calendar, plant, emptyPlant, task, emptyTask
  }

  enum Section {
    case calendar, tasks([Task]), plants([Plant])

    var sectionCells: [Row] {
      switch self {
      case .calendar:
        return [.calendar]

      case .tasks(let tasks):
        if(tasks.isEmpty) { return [.emptyTask] }
        var cells: [Row] = []
        for _ in tasks {
          cells.append(.task)
        }
        return cells

      case .plants(let plants):
        if(plants.isEmpty) { return [.plant] }
        var cells: [Row] = []
        for _ in plants {
          cells.append(.plant)
        }
        return cells
      }
    }
  }

  func addTasksSection(tasks: [Task]) {
    self.sections[1] = .tasks(tasks)
  }

  func addPlantsSection(plants: [Plant]) {
    self.sections[2] = .plants(plants)
  }
}
