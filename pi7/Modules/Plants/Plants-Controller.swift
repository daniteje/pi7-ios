//
//  Plants-Controller.swift
//  pi7
//
//  Created by Dani Tejedor on 27/3/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import UIKit

class PlantsViewController: ViewController, ViewModelController, PlantsCalendarRowDelegate {
  func setSelectedDay(day: Int) {
    viewModel.weekSchedule = viewModel.weekSchedule.map({$0.isSelected = false ; return $0})
    viewModel.weekSchedule[day].isSelected = true
    viewModel.selectedDayIndex = day
  }

  typealias T = PlantsViewModel

  // MARK: - IBOutlets
  @IBOutlet weak var tableView: UITableView!

  // MARK: - Properties
  var viewModel: PlantsViewModel!

  override var useLargeTitle: Bool {
    return true
  }

  // MARK: - Life cycle
  override func viewDidLoad() {
    super.viewDidLoad()

    setupUI()
    fillUI()
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
  }

  // MARK: - Functions
  func setupUI() {
    let rightButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
    rightButton.tintColor = UIColor(named: "pink")
    navigationItem.rightBarButtonItem = rightButton

    configure(tableView)
  }

  func fillUI() {
    if !isViewLoaded { return }

    navigationItem.title = String.Plants.Title.localized

    viewModel._selectedDayIndex.bind { day in
      self.viewModel.tableManager.addTasksSection(tasks: self.viewModel.weekSchedule[day].tasks)
      self.viewModel.tableManager.addPlantsSection(plants: self.viewModel.plants)

      self.tableView.reloadData()
    }

  }

  // MARK: - Observers
  @objc func addTapped(){
    let addPlantVM = AddPlantViewModel()
    let addPlantVC = ViewController.instantiate(viewController: AddPlantViewController.self, withViewModel: addPlantVM)
    present(addPlantVC, animated: true)
  }
}
