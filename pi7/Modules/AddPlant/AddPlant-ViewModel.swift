//
//  AddPlant-ViewModel.swift
//  pi7
//
//  Created by Dani Tejedor on 6/4/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import Foundation

class AddPlantViewModel: ViewModel {
  //MARK: - Properties
  let collectionManager = AddPlantCollectionManager()

  var _selectedPage: Dynamic<Int> = Dynamic(0)
  var selectedPage: Int {
    get { _selectedPage.value }
    set { _selectedPage.value = newValue }
  }
  //MARK: - Init
  init(){

  }
}
