//
//  PlantSchedulePage-ViewModel.swift
//  pi7
//
//  Created by Dani Tejedor on 11/4/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import Foundation

class PlantSchedulePageViewModel: ViewModel {
  // MARK: - Properties
  var _needsWatering: Dynamic<Bool> = Dynamic(false)
  var needsWatering: Bool {
    get { _needsWatering.value }
    set { _needsWatering.value = newValue }
  }

  var _needsFertilizant: Dynamic<Bool> = Dynamic(false)
  var needsFertilizant: Bool {
    get { _needsFertilizant.value }
    set { _needsFertilizant.value = newValue }
  }

  var _needsTrasplant: Dynamic<Bool> = Dynamic(false)
  var needsTrasplant: Bool {
    get { _needsTrasplant.value }
    set { _needsTrasplant.value = newValue }
  }
  
  // MARK: - Init
  init() {
  }

}
