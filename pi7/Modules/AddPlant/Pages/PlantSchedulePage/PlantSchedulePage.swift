//
//  PlantSchedulePage.swift
//  pi7
//
//  Created by Dani Tejedor on 11/4/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import UIKit

class PlantSchedulePage: UICollectionViewCell, ViewModelCell {
  typealias T = PlantSchedulePageViewModel

  // MARK: - Outlets
  @IBOutlet weak var questionLabel: UILabel!

  @IBOutlet weak var waterRemindMeLabel: UILabel!
  @IBOutlet weak var fertilizantRemindMeLabel: UILabel!
  @IBOutlet weak var transplantRemindMeLabel: UILabel!

  @IBOutlet weak var trasplantCheckImageView: UIImageView!
  @IBOutlet weak var fertilizantCheckImageView: UIImageView!
  @IBOutlet weak var waterCheckImageView: UIImageView!

  @IBOutlet weak var waterLabel: UILabel!
  @IBOutlet weak var fertilizantLabel: UILabel!
  @IBOutlet weak var trasplantLabel: UILabel!

  @IBOutlet weak var waterTextField: UITextField!
  @IBOutlet weak var fertilizantTextField: UITextField!
  @IBOutlet weak var trasplantTextField: UITextField!

  @IBOutlet weak var waterTimeIntervalButton: CustomButton!
  @IBOutlet weak var fertilizantTimeIntervalButton: CustomButton!
  @IBOutlet weak var trasplantTimeIntervalButton: CustomButton!

  @IBOutlet weak var waterStackView: UIStackView!
  @IBOutlet weak var fertilizantStackView: UIStackView!
  @IBOutlet weak var trasplantStackView: UIStackView!

  @IBOutlet weak var waterTimeIntervalSegmentedControl: UISegmentedControl!
  @IBOutlet weak var fertilizantTimeIntervalSegmentedControl: UISegmentedControl!
  @IBOutlet weak var trasplantTimeIntervalSegmentedControl: UISegmentedControl!

  // MARK: - Properties
  var viewModel: PlantSchedulePageViewModel! {
    didSet {
      setupUI()
      fillUI()
    }
  }


  // MARK: - Functions
  func setupUI() {
    questionLabel.text = String.AddPlant.ScheduleQuestion.localized

    waterLabel.text = String.AddPlant.NeedsWatering.localized
    fertilizantLabel.text = String.AddPlant.NeedsFertilizant.localized
    trasplantLabel.text = String.AddPlant.NeedsTrasplant.localized

    waterRemindMeLabel.text = String.AddPlant.Remind.localized
    fertilizantRemindMeLabel.text = String.AddPlant.Remind.localized
    transplantRemindMeLabel.text = String.AddPlant.Remind.localized

    configureSegmentControl(waterTimeIntervalSegmentedControl)
    configureSegmentControl(fertilizantTimeIntervalSegmentedControl)
    configureSegmentControl(trasplantTimeIntervalSegmentedControl)

    waterTimeIntervalButton.setTitle(waterTimeIntervalSegmentedControl.titleForSegment(at: 0), for: .normal)
    fertilizantTimeIntervalButton.setTitle(fertilizantTimeIntervalSegmentedControl.titleForSegment(at: 0), for: .normal)
    trasplantTimeIntervalButton.setTitle(trasplantTimeIntervalSegmentedControl.titleForSegment(at: 0), for: .normal)
    
    waterTimeIntervalSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
    waterTimeIntervalSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .selected)
    fertilizantTimeIntervalSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
    fertilizantTimeIntervalSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .selected)
    trasplantTimeIntervalSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
    trasplantTimeIntervalSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .selected)
  }
  

  func configureSegmentControl(_ segmentedControl: UISegmentedControl) {
    segmentedControl.setTitle(String.AddPlant.Days.localized, forSegmentAt: 0)
    segmentedControl.setTitle(String.AddPlant.Weeks.localized, forSegmentAt: 1)
    segmentedControl.setTitle(String.AddPlant.Months.localized, forSegmentAt: 2)
  }

  func fillUI() {
    viewModel._needsWatering.bind { needs in
      self.hideAllSelectors()

      let imageName = needs ? "rectangle-fill-pink" : "rectangle-pink"
      self.waterCheckImageView.image = UIImage(named: imageName)

      self.waterStackView.alpha = needs ? 1 : 0.5
      self.waterStackView.isUserInteractionEnabled = needs

    }

    viewModel._needsFertilizant.bind { needs in
      self.hideAllSelectors()

      let imageName = needs ? "rectangle-fill-pink" : "rectangle-pink"
      self.fertilizantCheckImageView.image = UIImage(named: imageName)

      self.fertilizantStackView.alpha = needs ? 1 : 0.5
      self.fertilizantStackView.isUserInteractionEnabled = needs

    }

    viewModel._needsTrasplant.bind { needs in
      self.hideAllSelectors()

      let imageName = needs ? "rectangle-fill-pink" : "rectangle-pink"
      self.trasplantCheckImageView.image = UIImage(named: imageName)

      self.trasplantStackView.alpha = needs ? 1 : 0.5
      self.trasplantStackView.isUserInteractionEnabled = needs
    }

  }

  func hideAllSelectors() {
    self.trasplantTimeIntervalSegmentedControl.isHidden   = true
    self.waterTimeIntervalSegmentedControl.isHidden       = true
    self.fertilizantTimeIntervalSegmentedControl.isHidden = true
  }


  // MARK: - Observers
  @IBAction func waterNeededToggle(_ sender: Any) {
    viewModel.needsWatering.toggle()
  }

  @IBAction func fertilizantNeededToggle(_ sender: Any) {
    viewModel.needsFertilizant.toggle()
  }


  @IBAction func trasplantNeededToggle(_ sender: Any) {
    viewModel.needsTrasplant.toggle()
  }

  @IBAction func selectWaterIntervalButtonPressed(_ sender: Any) {
    UIView.animate(withDuration: 0.25) {
      self.waterTimeIntervalSegmentedControl.isHidden.toggle()
    }

    self.fertilizantTimeIntervalSegmentedControl.isHidden = true
    self.trasplantTimeIntervalSegmentedControl.isHidden   = true
  }

  @IBAction func selectFertilizantIntervalButtonPressed(_ sender: Any) {
    UIView.animate(withDuration: 0.25) {
      self.fertilizantTimeIntervalSegmentedControl.isHidden.toggle()
    }

    self.waterTimeIntervalSegmentedControl.isHidden       = true
    self.trasplantTimeIntervalSegmentedControl.isHidden   = true
  }

  @IBAction func selectTrasplantIntervalButtonPreseed(_ sender: Any) {
    UIView.animate(withDuration: 0.25) {
      self.trasplantTimeIntervalSegmentedControl.isHidden.toggle()
    }

    self.waterTimeIntervalSegmentedControl.isHidden       = true
    self.fertilizantTimeIntervalSegmentedControl.isHidden = true
  }

  @IBAction func waterTimeIntervalChanged(_ sender: Any) {
    let index = waterTimeIntervalSegmentedControl.selectedSegmentIndex
    waterTimeIntervalButton.setTitle(waterTimeIntervalSegmentedControl.titleForSegment(at: index), for: .normal)
  }

  @IBAction func fertilizantTimeIntervalChanged(_ sender: Any) {
    let index = fertilizantTimeIntervalSegmentedControl.selectedSegmentIndex
    fertilizantTimeIntervalButton.setTitle(fertilizantTimeIntervalSegmentedControl.titleForSegment(at: index), for: .normal)
  }

  @IBAction func trasplantTimeIntervalChanged(_ sender: Any) {
    let index = trasplantTimeIntervalSegmentedControl.selectedSegmentIndex
    trasplantTimeIntervalButton.setTitle(trasplantTimeIntervalSegmentedControl.titleForSegment(at: index), for: .normal)
  }
}
