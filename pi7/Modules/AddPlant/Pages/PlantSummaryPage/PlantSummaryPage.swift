//
//  PlantSummaryPage.swift
//  pi7
//
//  Created by Dani Tejedor on 11/4/21.
//  Copyright © 2021 Rudo. All rights reserved.
//
import UIKit

class PlantSummaryPage: UICollectionViewCell, ViewModelCell {
  typealias T = PlantSummaryPageViewModel

  // MARK: - Outlets
  @IBOutlet weak var plantPictureImageView: UIImageView!
  @IBOutlet weak var questionLabel: UILabel!
  @IBOutlet weak var plantNameLabel: UILabel!
  @IBOutlet weak var plantTypeLabel: UILabel!
  @IBOutlet weak var uploadOwnImageButton: UIButton!

  @IBOutlet weak var transplantLabel: UILabel!
  @IBOutlet weak var transplantDateLabel: UILabel!
  @IBOutlet weak var fertilizingLabel: UILabel!
  @IBOutlet weak var fertilizantDateLabel: UILabel!
  @IBOutlet weak var wateringLabel: UILabel!
  @IBOutlet weak var wateringDateLabel: UILabel!

  // MARK: - Properties
  var viewModel: PlantSummaryPageViewModel! {
    didSet { fillUI() }
  }


  // MARK: - Functions
  func fillUI() {
    questionLabel.text = String.AddPlant.SummaryQuestion.localized
    wateringLabel.text = String.Plants.Watering.localized
    fertilizingLabel.text = String.Plants.Fertilizing.localized
    transplantLabel.text = String.Plants.Trasplanting.localized
    uploadOwnImageButton.setTitle(String.AddPlant.Upload.localized, for: .normal)
  }

  // MARK: - Observers
}
