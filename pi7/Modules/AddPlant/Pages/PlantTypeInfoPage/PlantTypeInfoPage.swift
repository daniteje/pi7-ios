//
//  AddPlant.swift
//  pi7
//
//  Created by Dani Tejedor on 9/4/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import UIKit

class PlantTypeInfoPage: UICollectionViewCell, ViewModelCell {
  typealias T = PlantTypeInfoPageViewModel

  // MARK: - Outlets
  @IBOutlet weak var plantTypeQuestionLabel: UILabel!
  @IBOutlet weak var plantsTypesCollectionView: UICollectionView!
  
  // MARK: - Properties
  var selecteds: [Bool] = []
  var viewModel: PlantTypeInfoPageViewModel! {
    didSet { fillUI() }
  }

  // MARK: - Functions
  func fillUI() {
    plantTypeQuestionLabel.text = String.AddPlant.NameQuestion.localized
    configure(plantsTypesCollectionView)
    for _ in 0...7 {
      selecteds.append(false)
    }
  }

  // MARK: - Observers
}

extension PlantTypeInfoPage: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
  func configure(_ collectionView: UICollectionView) {
    collectionView.dataSource = self
    collectionView.delegate = self

    collectionView.register(PlantTypeCell.self)

    collectionView.collectionViewLayout = createLayout()
  }

  private func createLayout() -> UICollectionViewLayout {
    let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(0.8))

    let item = NSCollectionLayoutItem(layoutSize: itemSize)
    item.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 10)

    let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(80))

    let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: 2)
    group.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0)

    let section = NSCollectionLayoutSection(group: group)

    return UICollectionViewCompositionalLayout(section: section)
  }

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    selecteds.count
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeue(PlantTypeCell.self, for: indexPath)
    cell.button.alpha = selecteds[indexPath.row] ? 1 : 0.5
    return cell
  }

  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    selecteds = selecteds.map({ _ in return false })
    selecteds[indexPath.row] = true
    collectionView.reloadData()
  }
}
