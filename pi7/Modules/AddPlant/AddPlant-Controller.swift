//
//  AddPlant.swift
//  pi7
//
//  Created by Dani Tejedor on 6/4/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import UIKit

class AddPlantViewController: ViewController, ViewModelController, UITextFieldDelegate {
  typealias T = AddPlantViewModel

  // MARK: - IBOutlets
  @IBOutlet weak var nameQuestionLabel: UILabel!
  @IBOutlet weak var nameTextField: UITextField!
  @IBOutlet weak var progressBar: UIProgressView!
  @IBOutlet weak var nextButton: CustomButton!
  @IBOutlet weak var backButton: CustomButton!
  @IBOutlet weak var collectionView: UICollectionView!
  
  // MARK: - Properties
  var viewModel: AddPlantViewModel!

  // MARK: - Life cycle
  override func viewDidLoad() {
    super.viewDidLoad()

    setupUI()
    fillUI()
    configure(collectionView)
  }

  // MARK: - Functions
  func setupUI() {
    nameQuestionLabel.text = String.AddPlant.NameQuestion.localized
    nameTextField.placeholder = String.AddPlant.NamePlaceholder.localized
    backButton.setTitle(String.Common.Back.localized, for: .normal)
  }

  func fillUI() {
    if !isViewLoaded { return }

    viewModel._selectedPage.bind { [weak self] newPage in
      guard let self = self else { return }

      var progress = Float(newPage) / Float(self.viewModel.collectionManager.sections.count)
      if(progress == 0) { progress = 0.01 }
      self.progressBar.setProgress(progress, animated: true)

      self.collectionView.isPagingEnabled = false
      self.collectionView.scrollToItem(at: IndexPath(row: newPage, section: 0), at: .left, animated: true)
      self.collectionView.isPagingEnabled = true

      let isLast = newPage == self.viewModel.collectionManager.sections.count - 1
      let textOnButton = isLast ? String.Common.Confirm.localized : String.Common.Next.localized
      UIView.animate(withDuration: 0.25) {
        self.backButton.isHidden = newPage == 0
        self.nextButton.setTitle(textOnButton, for: .normal)
      }
    }
  }

  // MARK: - Observers
  @IBAction func backButtonPressed(_ sender: Any) {
    guard viewModel.selectedPage > 0 else {
      return
    }
    viewModel.selectedPage -= 1
  }

  @IBAction func continueButtonPressed(_ sender: Any) {
    if(viewModel.selectedPage < self.viewModel.collectionManager.sections.count - 1) {
      viewModel.selectedPage += 1
    } else {
      //TODO: Add plant to list
      self.progressBar.setProgress(1, animated: true)
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
        self.dismiss(animated: true)
      }
    }
  }
}


extension AddPlantViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
  func configure(_ collectionView: UICollectionView) {
    collectionView.delegate   = self
    collectionView.dataSource = self

    collectionView.register(PlantTypeInfoPage.self)
    collectionView.register(PlantSchedulePage.self)
    collectionView.register(PlantSummaryPage.self)
  }

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    viewModel.collectionManager.sections.count
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cellType = viewModel.collectionManager.sections[indexPath.row]
    switch cellType {
    case .info:
      let cellVM = PlantTypeInfoPageViewModel()
      let cell = collectionView.dequeue(PlantTypeInfoPage.self, for: indexPath, viewModel: cellVM)
      return cell

    case .schedule:
      let cellVM = PlantSchedulePageViewModel()
      let cell = collectionView.dequeue(PlantSchedulePage.self, for: indexPath, viewModel: cellVM)
      return cell

    case .confirm:
      let cellVM = PlantSummaryPageViewModel()
      let cell = collectionView.dequeue(PlantSummaryPage.self, for: indexPath, viewModel: cellVM)
      return cell
    }
  }

  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    return
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return collectionView.frame.size
  }
}

class AddPlantCollectionManager {
  enum Row {
    case info, schedule, confirm
  }

  var sections: [Row] = [.info, .schedule, .confirm]
}
