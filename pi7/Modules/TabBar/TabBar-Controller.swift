//
//  TabBar-Controller.swift
//  pi7
//
//  Created by Dani Tejedor on 26/3/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController, UITabBarControllerDelegate {
  override func viewDidLoad() {
    super.viewDidLoad()

  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)

    let tabBarItems = tabBar.items! as [UITabBarItem]

    for (index, item) in tabBarItems.enumerated() {
      setNavigation(index, item)
    }

    self.selectedIndex = TabBarItem.cart.rawValue
  }

  // MARK: - Functions
  func setNavigation(_ index: Int, _ item: UITabBarItem) {
    let nav = viewControllers![index] as? UINavigationController

    switch TabBarItem(rawValue: index) {
    case .cart:
      tabBar.items?[index].title = String.TabBar.Cart.localized
      let cartVM = CartViewModel()
      let cartVC = UIViewController.instantiate(viewController: CartViewController.self, withViewModel: cartVM)
      nav?.viewControllers = [cartVC]

    case .plants:
      tabBar.items?[index].title = String.TabBar.Plants.localized
      let plantsVM = PlantsViewModel()
      let plantsVC = UIViewController.instantiate(viewController: PlantsViewController.self, withViewModel: plantsVM)
      nav?.viewControllers = [plantsVC]

    case .status:
      tabBar.items?[index].title = String.TabBar.Status.localized
      let statusVM = StatusViewModel()
      let statusVC = UIViewController.instantiate(viewController: StatusViewController.self, withViewModel: statusVM)
      nav?.viewControllers = [statusVC]

    case .profile:
      tabBar.items?[index].title = String.TabBar.Profile.localized
      let profileVM = ProfileViewModel()
      let profileVC = UIViewController.instantiate(viewController: ProfileViewController.self, withViewModel: profileVM)
      nav?.viewControllers = [profileVC]

    case .none:
      break;
    }
}
}
