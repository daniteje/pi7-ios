//
//  CustomTextField-Models.swift
//  pi7
//
//  Created by Dani Tejedor on 25/03/2021.
//  Copyright © 2021 Dani Teje. All rights reserved.
//

import UIKit

class TextFieldError {
  let localizedDescription: String
  let checkCondition: ((String) -> Bool)

  init(localizedDescription: String, checkCondition: @escaping ((String) -> Bool)) {
    self.localizedDescription = localizedDescription
    self.checkCondition       = checkCondition
  }
}

class TextFieldErrorMinimumCharacters: TextFieldError {
  let minCharacters: Int

  init(_ minCharacters: Int) {
    self.minCharacters = minCharacters

    super.init(localizedDescription: "Minimum \(minCharacters) characters") { (value) -> Bool in
      return value.count < minCharacters
    }
  }
}

class TextFieldErrorEmailFormat: TextFieldError {
  init() {
    super.init(localizedDescription: "Invalid email format") { (value) -> Bool in
      return !value.isValidEmail()
    }
  }
}

class TextFieldErrorEmptyValue: TextFieldError {
  init() {
    super.init(localizedDescription: "Empty field") { (value) -> Bool in
      return value.isEmpty
    }
  }
}
