//
//  CartCategoryHeader.swift
//  pi7
//
//  Created by Dani Tejedor on 28/3/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import UIKit

class CartCategoryTableViewHeader: UITableViewHeaderFooterView, ViewModelCell, Reusable {
  typealias T = CartCategoryTableViewHeaderViewModel

  // MARK: - Outlets
  @IBOutlet weak var categoryName: UILabel!

  // MARK: - Properties
  var viewModel: CartCategoryTableViewHeaderViewModel! {
    didSet { fillUI() }
  }


  // MARK: - Functions
  func fillUI() {
    categoryName.text = viewModel.title
  }

  // MARK: - Observers
}
