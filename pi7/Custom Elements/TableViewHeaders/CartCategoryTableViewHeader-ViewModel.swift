//
//  CartCategory-ViewModel.swift
//  pi7
//
//  Created by Dani Tejedor on 28/3/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import Foundation

class CartCategoryTableViewHeaderViewModel: ViewModel {
  let title: String

  init(title: String){
    self.title = title
  }
}
