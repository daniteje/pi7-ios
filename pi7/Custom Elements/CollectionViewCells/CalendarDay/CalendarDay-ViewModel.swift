//
//  CalendarDay-ViewModel.swift
//  pi7
//
//  Created by Dani Tejedor on 31/3/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import Foundation

class CalendarDayCellViewModel: ViewModel {

  // MARK: - Properties
  let dayNumber: Int
  var isSelected: Bool
  let hasTask: Bool
  let dayLetter: String
  let isToday: Bool
  
  // MARK: - Init
  init(day: Day, isToday: Bool = false) {
    self.dayNumber = day.date.day
    self.isSelected = day.isSelected
    self.hasTask = day.hasTask
    self.dayLetter = day.date.weekdayStringCode
    self.isToday = isToday
  }
}
