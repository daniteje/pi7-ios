//
//  CalendarDay.swift
//  pi7
//
//  Created by Dani Tejedor on 31/3/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import UIKit

class CalendarDay: UICollectionViewCell, ViewModelCell {
  typealias T = CalendarDayCellViewModel

  // MARK: - Outlets
  @IBOutlet weak var dayLabel: UILabel!
  @IBOutlet weak var letterLabel: UILabel!
  @IBOutlet weak var backgroundImage: UIImageView!
  @IBOutlet weak var taskIndicator: UIView!

  // MARK: - Properties
  var viewModel: CalendarDayCellViewModel! {
    didSet { fillUI() }
  }

  // MARK: - Functions
  func fillUI() {
    dayLabel.text = String(viewModel.dayNumber)
    letterLabel.text = viewModel.dayLetter

    backgroundImage.image = viewModel.isSelected ?
      UIImage(named: "rectangle-fill-pink") :
      UIImage(named: "rectangle-fill-black")
    taskIndicator.backgroundColor = viewModel.isSelected ? .black : UIColor(named: "blue")
    taskIndicator.isHidden = !viewModel.hasTask
    dayLabel.textColor = viewModel.isSelected ? .black : .white
    letterLabel.textColor = viewModel.isToday ? UIColor(named: "pink") : .white
  }
}
