//
//  PlantType.swift
//  pi7
//
//  Created by Dani Tejedor on 6/4/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import UIKit

class PlantTypeCell: UICollectionViewCell, ViewModelCell {
  typealias T = PlantTypeCellViewModel

  // MARK: - Outlets
  @IBOutlet weak var button: CustomButton!
  
  // MARK: - Properties
  var viewModel: PlantTypeCellViewModel! {
    didSet { fillUI() }
  }


  // MARK: - Functions
  func fillUI() {
  }

  // MARK: - Observers
}
