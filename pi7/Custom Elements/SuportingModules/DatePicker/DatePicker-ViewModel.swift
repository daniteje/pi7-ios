//
//  DatePicker-ViewModel.swift
//  HCB
//
//  Created by Dani Teje on 23/9/20.
//  Copyright © 2021 Dani Teje. All rights reserved.
//

import Foundation

class DatePickerViewModel: ViewModel {
  // MARK: - Properties
  let title: String

  // MARK: - Init
  init(title: String) {
    self.title = title
  }
}
