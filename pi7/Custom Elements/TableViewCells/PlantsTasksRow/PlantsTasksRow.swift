//
//  PlantsTasksRow.swift
//  pi7
//
//  Created by Dani Tejedor on 2/4/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import UIKit

class PlantsTasksRow: UITableViewCell, ViewModelCell {
  typealias T = PlantsTasksRowViewModel

  // MARK: - Outlets
  @IBOutlet weak var tableView: UITableView!

  // MARK: - Properties
  var viewModel: PlantsTasksRowViewModel! {
    didSet { fillUI() }
  }


  // MARK: - Functions
  func fillUI() {
    configure(tableView)

    tableView.reloadData()
  }

  // MARK: - Observers
}
