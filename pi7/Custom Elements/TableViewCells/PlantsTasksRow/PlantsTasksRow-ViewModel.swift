//
//  PlantsTasksRow-ViewModel.swift
//  pi7
//
//  Created by Dani Tejedor on 2/4/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import Foundation

class PlantsTasksRowViewModel: ViewModel {
  // MARK: - Properties
  var _tasks: Dynamic<[Task]> = Dynamic([])
  var tasks: [Task] {
    get { _tasks.value }
    set { _tasks.value = newValue }
  }

  // MARK: - Init
  init(tasks: [Task] = []) {
    self._tasks = Dynamic(tasks)
  }
}
