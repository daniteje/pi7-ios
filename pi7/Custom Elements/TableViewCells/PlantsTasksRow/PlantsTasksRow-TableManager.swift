//
//  PlantsTasksRow-TableManager.swift
//  pi7
//
//  Created by Dani Tejedor on 2/4/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import UIKit

extension PlantsTasksRow: UITableViewDelegate, UITableViewDataSource {
  func configure(_ tableView: UITableView){
    tableView.delegate = self
    tableView.dataSource = self

    tableView.register(PlantsTasksEmptyRow.self)
    tableView.register(PlantsTasksCell.self)
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.tasks.isEmpty ? 1 : viewModel.tasks.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if (viewModel.tasks.isEmpty) {
      return tableView.dequeue(PlantsTasksEmptyRow.self)
    }
    let tasksVM = PlantsTasksCellViewModel(task: viewModel.tasks[indexPath.row])
    return tableView.dequeue(PlantsTasksCell.self, viewModel: tasksVM)
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    viewModel.tasks[indexPath.row].isDone.toggle()
    let element = viewModel.tasks.remove(at: indexPath.row)
    if(element.isDone) {
      viewModel.tasks.append(element)
    } else {
      viewModel.tasks.insert(element, at: 0)
    }
    tableView.reloadData()
  }

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
}
