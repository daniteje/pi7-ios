//
//  PlantsTasksEmptyRow.swift
//  pi7
//
//  Created by Dani Tejedor on 2/4/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import UIKit

class PlantsTasksEmptyRow: UITableViewCell, Reusable {
  // MARK: - Outlets
  @IBOutlet weak var noTasksLabel: UILabel!

  // MARK: - Functions
  override func awakeFromNib() {
    setupUI()
  }

  func setupUI() {
    noTasksLabel.text = String.Plants.NoTasks.localized
  }
}
