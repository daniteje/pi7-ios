//
//  CartSectionCell-ViewModel.swift
//  pi7
//
//  Created by Dani Tejedor on 29/3/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import Foundation

class CartSectionCellViewModel: ViewModel {
  // MARK: - Properties
  let section: CartSection
  var _isSelected: Dynamic<Bool> = Dynamic(false)
  var isSelected: Bool {
    get { _isSelected.value }
    set { _isSelected.value = newValue }
  }
  
  // MARK: - Init
  init(section: CartSection, isSelected: Bool = false) {
    self.section = section
    self._isSelected = Dynamic(isSelected)
  }

}
