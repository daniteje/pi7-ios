//
//  CartSectionCell.swift
//  pi7
//
//  Created by Dani Tejedor on 29/3/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import UIKit

class CartSectionCell: UITableViewCell, ViewModelCell {
  typealias T = CartSectionCellViewModel

  // MARK: - Outlets
  @IBOutlet weak var sectionButton: CustomButton!

  // MARK: - Properties
  var viewModel: CartSectionCellViewModel! {
    didSet { fillUI() }
  }


  // MARK: - Functions
  func fillUI() {
    sectionButton.setTitle(viewModel.section.sectionName, for: .normal)
    viewModel._isSelected.bindAndFire { isSelected in
      let borderColor = isSelected ? UIColor(named: "pink") : .darkGray
      self.sectionButton.borderColor = borderColor ?? .clear
    }
  }

  // MARK: - Observers
}
