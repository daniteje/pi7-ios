//
//  CartCell-ViewModel.swift
//  pi7
//
//  Created by Dani Tejedor on 27/3/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import Foundation

class CartCellViewModel: ViewModel {
  // MARK: - Properties
  var _product: Dynamic<Product>
  var product: Product {
    get { _product.value }
    set { _product.value = newValue }
  }

  // MARK: - Init
  init(product: Product) {
    self._product = Dynamic(product)
  }
}
