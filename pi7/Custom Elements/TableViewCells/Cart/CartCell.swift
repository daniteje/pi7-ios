//
//  CartCell.swift
//  pi7
//
//  Created by Dani Tejedor on 27/3/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import UIKit

class CartCell: UITableViewCell, ViewModelCell {
  typealias T = CartCellViewModel

  // MARK: - Outlets
  @IBOutlet weak var isCheckedImage: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!

  // MARK: - Properties
  var viewModel: CartCellViewModel! {
    didSet { fillUI() }
  }


  // MARK: - Functions
  func fillUI() {
    viewModel._product.bindAndFire { product in
      self.nameLabel.text = self.viewModel.product.name
      self.isCheckedImage.image = product.isChecked ? UIImage(named: "done-pink") : UIImage(named: "pink-checkbox")
      self.nameLabel.alpha = product.isChecked ? 0.5 : 1
    }
  }

  // MARK: - Observers
}
