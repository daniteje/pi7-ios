//
//  PlantsCalendarRow-ViewModel.swift
//  pi7
//
//  Created by Dani Tejedor on 1/4/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import Foundation

class PlantsCalendarRowViewModel: ViewModel {
  // MARK: - Properties
  let weekSchedule: [Day]
  var selectedDayIndex: Int
  
  // MARK: - Init
  init(weekSchedule: [Day], selected selectedDayIndex: Int) {
    self.weekSchedule = weekSchedule
    self.selectedDayIndex = selectedDayIndex
  }
}
