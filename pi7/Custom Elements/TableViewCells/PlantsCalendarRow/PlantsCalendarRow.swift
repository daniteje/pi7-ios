//
//  PlantsCalendarRow.swift
//  pi7
//
//  Created by Dani Tejedor on 1/4/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import UIKit

class PlantsCalendarRow: UITableViewCell, ViewModelCell {
  typealias T = PlantsCalendarRowViewModel

  // MARK: - Outlets
  @IBOutlet weak var calendarCollectionView: UICollectionView!
  weak var delegate: PlantsCalendarRowDelegate?
  
  // MARK: - Properties
  var viewModel: PlantsCalendarRowViewModel! {
    didSet { fillUI() }
  }


  // MARK: - Functions
  func fillUI() {
    configure(calendarCollectionView)
    calendarCollectionView.reloadData()
  }

  // MARK: - Observers
}
