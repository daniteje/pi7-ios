//
//  PlantsCalendarRow-CollectionManager.swift
//  pi7
//
//  Created by Dani Tejedor on 1/4/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import UIKit

protocol PlantsCalendarRowDelegate: class {
  func setSelectedDay(day: Int)
}

extension PlantsCalendarRow: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
  func configure(_ collectionView: UICollectionView){
    collectionView.dataSource = self
    collectionView.delegate = self

    collectionView.register(CalendarDay.self)
  }

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    viewModel.weekSchedule.count
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let day = viewModel.weekSchedule[indexPath.row]
    let viewModel = CalendarDayCellViewModel(day: day, isToday: Calendar.current.isDateInToday(day.date))
    return collectionView.dequeue(CalendarDay.self, for: indexPath, viewModel: viewModel)
  }

  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    viewModel.selectedDayIndex = indexPath.row
    delegate?.setSelectedDay(day: indexPath.row)
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: (screenWidth-40)/CGFloat(2 + viewModel.weekSchedule.count), height: 100)
  }



  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

      //Where elements_count is the count of all your items in that
      //Collection view...
    let cellCount = CGFloat(viewModel.weekSchedule.count)

      //If the cell count is zero, there is no point in calculating anything.
      if cellCount > 0 {
          let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
          let cellWidth = flowLayout.itemSize.width + flowLayout.minimumInteritemSpacing

          //20.00 was just extra spacing I wanted to add to my cell.
          let totalCellWidth = cellWidth*cellCount + 20.00 * (cellCount-1)
          let contentWidth = collectionView.frame.size.width - collectionView.contentInset.left - collectionView.contentInset.right

          if (totalCellWidth < contentWidth) {
              //If the number of cells that exists take up less room than the
              //collection view width... then there is an actual point to centering them.

              //Calculate the right amount of padding to center the cells.
              let padding = (contentWidth - totalCellWidth) / 2.0
              return UIEdgeInsets(top: 0, left: padding, bottom: 0, right: padding)
          } else {
              //Pretty much if the number of cells that exist take up
              //more room than the actual collectionView width, there is no
              // point in trying to center them. So we leave the default behavior.
              return UIEdgeInsets(top: 0, left: 40, bottom: 0, right: 40)
          }
      }
      return UIEdgeInsets.zero
  }

}
