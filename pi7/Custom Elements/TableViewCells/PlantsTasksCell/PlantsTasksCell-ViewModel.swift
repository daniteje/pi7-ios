//
//  PlantsTasksCell-ViewModel.swift
//  pi7
//
//  Created by Dani Tejedor on 2/4/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import Foundation

class PlantsTasksCellViewModel: ViewModel {
  // MARK: - Properties
  var _task: Dynamic<Task>
  var task: Task {
    get { _task.value }
    set { _task.value = newValue }
  }
  // MARK: - Init
  init(task: Task) {
    self._task = Dynamic(task)
  }

}
