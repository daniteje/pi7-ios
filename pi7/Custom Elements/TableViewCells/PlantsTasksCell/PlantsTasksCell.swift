//
//  PlantsTasksCell.swift
//  pi7
//
//  Created by Dani Tejedor on 2/4/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import UIKit

class PlantsTasksCell: UITableViewCell, ViewModelCell {
  typealias T = PlantsTasksCellViewModel

  // MARK: - Outlets
  @IBOutlet weak var checkImage: UIImageView!
  @IBOutlet weak var actionLabel: UILabel!
  @IBOutlet weak var plantLabel: UILabel!

  // MARK: - Properties
  var viewModel: PlantsTasksCellViewModel! {
    didSet { fillUI() }
  }


  // MARK: - Functions
  func fillUI() {
    self.checkImage.image = UIImage(named: "rectangle-black")

    viewModel._task.bindAndFire { task in
      switch task.type {
      case .waterPlant:
        self.actionLabel.text = String.Plants.Water.localized
        self.actionLabel.textColor = UIColor(named: "blue")
        self.checkImage.image = task.isDone ? UIImage(named: "done-blue") : UIImage(named: "rectangle-blue")

      case .fertilizant:
        self.actionLabel.text = String.Plants.Fertilize.localized
        self.actionLabel.textColor = UIColor(named: "green")
        self.checkImage.image = task.isDone ? UIImage(named: "done-green") : UIImage(named: "rectangle-green")

      case .transplant:
        self.actionLabel.text = String.Plants.Trasplant.localized
        self.actionLabel.textColor = UIColor(named: "yellow")
        self.checkImage.image = task.isDone ? UIImage(named: "done-yellow") : UIImage(named: "rectangle-yellow")
      }

      self.plantLabel.text = task.plant.alias
      self.actionLabel.alpha = task.isDone ? 0.5 : 1
      self.plantLabel.alpha = task.isDone ? 0.5 : 1
    }
  }
}
