//
//  PlantsListCell-ViewModel.swift
//  pi7
//
//  Created by Dani Tejedor on 2/4/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import Foundation

class PlantsListCellViewModel: ViewModel {
  // MARK: - Properties
  let plant: Plant
  // MARK: - Init
  init(plant: Plant) {
    self.plant = plant
  }

}
