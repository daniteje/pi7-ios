//
//  PlantsListCell.swift
//  pi7
//
//  Created by Dani Tejedor on 2/4/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import UIKit

class PlantsListCell: UITableViewCell, ViewModelCell {
  typealias T = PlantsListCellViewModel

  // MARK: - Outlets
  @IBOutlet weak var plantNameLabel: UILabel!
  @IBOutlet weak var plantTypeLabel: UILabel!
  @IBOutlet weak var wateringLabel: UILabel!
  @IBOutlet weak var wateringDate: UILabel!
  @IBOutlet weak var fertilizingLabel: UILabel!
  @IBOutlet weak var fertilizingDate: UILabel!
  @IBOutlet weak var picture: UIImageView!

  // MARK: - Properties
  var viewModel: PlantsListCellViewModel! {
    didSet { fillUI() }
  }


  // MARK: - Functions
  func fillUI() {
    wateringLabel.text = String.Plants.Watering.localized
    fertilizingLabel.text = String.Plants.Fertilizing.localized
  }
}
