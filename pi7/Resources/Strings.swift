//
//  Strings.swift
//  pi7
//
//  Created by Dani Tejedor on 25/4/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import Foundation

protocol Localizable: CustomStringConvertible {
  var rawValue: String { get }
}

extension Localizable {
  var localized: String {
    return NSLocalizedString(self.rawValue, comment: "")
  }

  var uppercased: String {
    return self.localized.uppercased()
  }

  var description: String {
    return self.localized
  }

  func localized(with: CVarArg...) -> String {
    let text = String(format: self.localized, arguments: with)
    return text
  }
}

extension String {
  enum Common: String, Localizable {
    case Yes = "Common_Yes"
    case No = "Common_No"
    case Next = "Common_Next"
    case Back = "Common_Back"
    case Return = "Common_Return"
    case Confirm = "Common_Confirm"
    case Cancel = "Common_Cancel"
    case Continue = "Common_Continue"
    case Mail = "Common_Mail"
    case Password = "Common_Password"
    case Name = "Common_Name"
    case Lastname = "Common_Lastname"
    case EmptyField = "Common_TextField_Empty"
    case General = "Common_General"
    case Login = "Common_Login"
    case See = "Common_See"
    case Change = "Common_Change"
    case Update = "Common_Update"
    case Exit = "Common_Exit"
    case Search = "Common_Search"
    case LogOut = "Common_LogOut"
  }

  enum Register: String, Localizable {
    case Title = "Register_Title"
    case ConfirmPassword = "Register_ConfirmPassword"
    case Button = "Register_Register"
  }

  enum RecoverPassword: String, Localizable {
    case Information = "RecoverPassword_Information"
    case Button = "RecoverPassword_Recover"
  }

  enum Login: String, Localizable {
    case Welcome = "Login_Welcome"
    case ForgotPassword = "Login_ForgotPassword"
    case Button = "Login_Login"
    case RegisterText = "Login_DontHaveAccount"
    case Register = "Login_Register"
    case ErrorBadRequest = "Login_ErrorBadRequest"
    case ErrorNotFound = "Login_ErrorNotFound"
    case ErrorUnknow = "Login_ErrorUnknow"
  }

  enum TabBar: String, Localizable {
    case Cart = "Tabs_Cart"
    case Plants = "Tabs_Plants"
    case Status = "Tabs_Status"
    case Profile = "Tabs_Profile"
  }

  enum Cart: String, Localizable {
    case Title = "Cart"
  }

  enum AddProduct: String, Localizable {
    case NameQuestion = "AddProduct_NameQuestion"
    case NamePlaceHolder = "AddProduct_NamePlaceholder"
    case ListQuestion = "AddProduct_ListQuestion"
  }

  enum Plants: String, Localizable {
    case Title = "Plants_Title"
    case NoTasks = "Plants_NoTasks"
    case Water  = "Plants_Water"
    case Fertilize = "Plants_Fertilize"
    case Trasplant = "Plants_Trasplant"
    case MyPlants = "Plants_MyPlants"
    case Watering = "Plants_Watering"
    case Fertilizing = "Plants_Fertilizing"
    case Trasplanting = "Plants_Trasplanting"
  }

  enum AddPlant: String, Localizable {
    case NameQuestion = "AddPlant_NameQuestion"
    case TypeQuestion = "AddPlant_TypeQuestion"
    case NamePlaceholder = "AddPlant_NamePlaceholder"
    case ScheduleQuestion = "AddPlant_ScheduleQuestion"
    case NeedsWatering = "AddPlant_NeedsWatering"
    case NeedsFertilizant = "AddPlant_NeedsFertilizant"
    case NeedsTrasplant = "AddPlant_NeedsTrasplant"
    case Remind = "AddPlant_RemindMe"
    case Days = "AddPlant_Days"
    case Weeks = "AddPlant_Weeks"
    case Months = "AddPlant_Months"
    case SummaryQuestion = "AddPlant_SummaryQuestion"
    case Upload = "AddPlant_UploadOwnPicture"
  }

  enum Profile: String, Localizable {
    case Code = "Profile_Code"
    case EditGroup = "Profile_EditGroup"
    case EditInfo = "Profile_EditInfo"
    case EditPassword = "Profile_EditPassword"
  }
}
