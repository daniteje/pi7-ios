//
//  Array.swift
//  pi7
//
//  Created by Dani Tejedor on 28/3/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import Foundation

extension Collection {
    func separate(predicate: (Iterator.Element) -> Bool) -> (matching: [Iterator.Element], notMatching: [Iterator.Element]) {
      var groups: ([Iterator.Element],[Iterator.Element]) = ([],[])
        for element in self {
            if predicate(element) {
                groups.0.append(element)
            } else {
                groups.1.append(element)
            }
        }
        return groups
    }
}
