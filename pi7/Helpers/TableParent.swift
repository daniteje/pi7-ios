//
//  TableParent.swift
//  pi7
//
//  Created by Dani Tejedor on 25/03/2021.
//  Copyright © 2021 Dani Teje. All rights reserved.
//

import UIKit

protocol TableParent: AnyObject {
  func reloadTable()
}
