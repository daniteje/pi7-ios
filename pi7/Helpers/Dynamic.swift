//
//  Dynamic.swift
//  pi7
//
//  Created by Dani Tejedor on 25/03/2021.
//  Copyright © 2021 Dani Teje. All rights reserved.
//

class Dynamic<T> {
  typealias Listener = (T) -> ()

  var listener: Listener?

  func bind(_ listener: Listener?) {
    self.listener = listener
  }

  func bindAndFire(_ listener: Listener?) {
    self.listener = listener
    listener?(value)
  }

  var value: T {
    didSet {
      listener?(value)
    }
  }

  init(_ v: T) {
    value = v
  }
}
