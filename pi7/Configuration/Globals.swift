//
//  Globals.swift
//  pi7
//
//  Created by Dani Teje on 26/03/2021.
//  Copyright © 2021 Dani Teje. All rights reserved.
//

import Foundation

/// Defines the paths to use
enum Endpoint {
  case backend

  var path: String {
    switch self {
    case .backend:
    return "https://translator.Dani Teje.es"
    }
  }
}

/// Defines the ContentTypes
struct ContentType {
  static let urlEncoded  = "application/x-www-form-urlencoded"
  static let jsonEncoded = "application/json"
}
