//
//  Configuration.swift
//  pi7
//
//  Created by Dani Tejedor on 26/3/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import Foundation

enum TabBarItem: Int {
  case cart
  case plants
  case status
  case profile
}

let products1 = [Product(name: "Patatas", isChecked: false, sectionID: 1),
                 Product(name: "Cebollas", isChecked: true, sectionID: 1),
                 Product(name: "Lechuga", isChecked: false, sectionID: 1)]
let products2 = [Product(name: "Papel higiénico", isChecked: false, sectionID: 2)]
let products3 = [Product(name: "Agua", isChecked: true, sectionID: 3),
                 Product(name: "Coca-Cola", isChecked: true, sectionID: 3),
                 Product(name: "Leche", isChecked: true, sectionID: 3),
                 Product(name: "Zumo de naranja", isChecked: false, sectionID: 3)]
let products4 = [Product(name: "Cereales", isChecked: false, sectionID: 4),
                 Product(name: "Pan", isChecked: false, sectionID: 4),
                 Product(name: "Tostadas", isChecked: false, sectionID: 4),
                 Product(name: "Azúcar", isChecked: false, sectionID: 4)]

let section1 = CartSection(sectionID: 1, sectionName: "Frutas y verduras", products: products1)
let section2 = CartSection(sectionID: 2, sectionName: "Higiene", products: products2)
let section3 = CartSection(sectionID: 3, sectionName: "Bebidas", products: products3)
let section4 = CartSection(sectionID: 4, sectionName: "Panadería", products: products4)

let generalSection = CartSection(sectionID: 0, sectionName: "General", products: [])

let calendar = Calendar.current
let addDay = calendar.date(byAdding: .day,  value: 1, to: Date())
let addDay2 = calendar.date(byAdding: .day, value: 1, to: addDay!)
let addDay3 = calendar.date(byAdding: .day, value: 1, to: addDay2!)
let addDay4 = calendar.date(byAdding: .day, value: 1, to: addDay3!)
let addDay5 = calendar.date(byAdding: .day, value: 1, to: addDay4!)

let plant1 = Plant(type: .cactus, alias: "Mauri", scientificName: nil, nextWatering: nil, nextFertilizant: nil, nextTrasplant: nil)
let plant2 = Plant(type: .fruit, alias: "Tomatera", scientificName: nil, nextWatering: nil, nextFertilizant: nil, nextTrasplant: nil)
let plant3 = Plant(type: .houseplant, alias: "Manolito", scientificName: nil, nextWatering: nil, nextFertilizant: nil, nextTrasplant: nil)

let day1 = Day(date: Date(), tasks: [Task(plant: plant2, type: .waterPlant, isDone: true),Task(plant: plant3, type: .fertilizant, isDone: false), Task(plant: plant1, type: .transplant, isDone: false)], isSelected: true)
let day2 = Day(date: addDay!, isSelected: false)
let day3 = Day(date: addDay2!, tasks: [Task(plant: plant2, type: .waterPlant), Task(plant: plant3, type: .waterPlant)], isSelected: false)
let day4 = Day(date: addDay3!, isSelected: false)
let day5 = Day(date: addDay4!, isSelected: false)
let day6 = Day(date: addDay5!, isSelected: false)

let week = [day1, day2, day3, day4, day5, day6]

let initialPlants = [plant1, plant2, plant3]
