//
//  Product.swift
//  pi7
//
//  Created by Dani Tejedor on 28/3/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import Foundation

class Product: Codable {
  //TODO: Replace when API is available
//  private enum CodingKeys: String, CodingKey {
//    case id
//    case name
//    case sectionID = "section_id"
//    case createdBy = "created_by"
//    case checked
//    case checkedBy = "checked_by"
//  }
//
//  var id: String
//  var name: String?
//  var sectionID: String?
//  var createdBy: String?
//  var checked: Bool?
//  var checkedBy: String?

  var sectionID: Int
  var name: String
//  var sectionID: Int
//  var sectionName: String
  var isChecked: Bool

  init(name: String, isChecked: Bool = false, sectionID: Int){
    self.sectionID = sectionID
    self.name = name
    self.isChecked = isChecked
  }
}
