//
//  UpdateRequired.swift
//  GoodPractices
//
//  Created by Dani Teje on 26/03/2021.
//  Copyright © 2021 Dani Teje. All rights reserved.
//

import Foundation

class UpdateRequired: Decodable {
  
  var updateRequired: Bool = false
}
