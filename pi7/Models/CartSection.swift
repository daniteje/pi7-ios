//
//  CartSection.swift
//  pi7
//
//  Created by Dani Tejedor on 28/3/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import Foundation

class CartSection: Codable {
  var sectionID: Int
  var sectionName: String
  var products: [Product]

  init(sectionID: Int, sectionName: String, products: [Product] = []){
    self.sectionID = sectionID
    self.sectionName = sectionName
    self.products = products
  }
}
