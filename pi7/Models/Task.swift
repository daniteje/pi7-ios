//
//  File.swift
//  pi7
//
//  Created by Dani Tejedor on 2/4/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import Foundation

class Task: Codable {
  let plant: Plant
  let type: TaskType
  var isDone: Bool

  init(plant: Plant, type: TaskType = .waterPlant, isDone: Bool = false){
    self.plant = plant
    self.type = type
    self.isDone = isDone
  }
}

enum TaskType: Int, Codable {
  case waterPlant
  case fertilizant
  case transplant
}
