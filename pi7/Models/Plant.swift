//
//  Plant.swift
//  pi7
//
//  Created by Dani Tejedor on 2/4/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import Foundation

class Plant: Codable {
  let id: String
  var type: PlantType
  var alias: String
  var scientificName: String?
  var nextWatering: Date?
  var nextFertilizant: Date?
  var nextTrasplant: Date?

  init(type: PlantType, alias: String, scientificName: String?,
       nextWatering: Date?, nextFertilizant: Date?, nextTrasplant: Date?){
    self.id = UUID().uuidString
    self.type = type
    self.alias = alias
    self.scientificName = scientificName
    self.nextWatering = nextWatering
    self.nextFertilizant = nextFertilizant
    self.nextTrasplant = nextTrasplant
  }
}

enum PlantType: String, Codable, CustomStringConvertible {
  var description: String {
    switch self {
    case .tree:       return "Árbol"
    case .bush:       return "Arbusto"
    case .aromatic:   return "Aromática"
    case .cactus:     return "Cactus"
    case .conifer:    return "Conífera"
    case .fruit:      return "Frutal"
    case .herb:       return "Hierba"
    case .vegetables: return "Verduras"
    case .palm:       return "Palmera"
    case .water:      return "Acuática"
    case .houseplant: return "De interior"
    case .creeper:    return "trepadora"
    case .rosebush:   return "Rosal"
    }
  }

  case tree, bush, aromatic, cactus,
       conifer, fruit, herb, vegetables,
       palm, water, houseplant, creeper, rosebush
  
}
