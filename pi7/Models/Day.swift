//
//  Day.swift
//  pi7
//
//  Created by Dani Tejedor on 1/4/21.
//  Copyright © 2021 Rudo. All rights reserved.
//

import Foundation

class Day: Codable {
  let date: Date
  var hasTask: Bool {
    !tasks.isEmpty
  }
  var tasks: [Task] = []
  var isSelected: Bool

  init(date: Date, tasks: [Task] = [], isSelected: Bool = false) {
    self.date = date
    self.tasks = tasks
    self.isSelected = isSelected
  }
}
